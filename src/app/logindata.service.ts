import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LogindataService {

  serverUrl = 'http://obscure-cove-12172.herokuapp.com/api/';

  constructor(private http: HttpClient) {
  }

  getPost(data) {
    return this.http.post(this.serverUrl + 'apilogin', data);
  }

  usersignup(data) {
    return this.http.post(this.serverUrl + 'userSignup', data);
  }
}
