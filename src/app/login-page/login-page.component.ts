import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LogindataService} from '../logindata.service';
import {ToastrService} from 'ngx-toastr';
import {error} from '@angular/compiler/src/util';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {


  model = {
    user: '',
    password: ''
  };

  user1: any;
  user2 = {
    user: '',
    password: ''
  };

  constructor(private route: ActivatedRoute, private router: Router,
              private loginService: LogindataService,
              private toastr: ToastrService) {
  }

  ngOnInit(): void {
  }

  getauth() {
    this.loginService.getPost(this.model).subscribe(hero => {
      this.user1.push(hero);
      this.router.navigate(['/dashboard']);
    }, err => {
      console.log(err);
    });
  }
}
