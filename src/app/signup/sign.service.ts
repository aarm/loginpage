import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SignService {
    SignupApi = ''
  constructor(private http: HttpClient) { }

  getsign(data) {
    return this.http.post(this.SignupApi  , data);
  }
}
